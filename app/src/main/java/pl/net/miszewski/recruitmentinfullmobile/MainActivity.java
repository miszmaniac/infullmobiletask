package pl.net.miszewski.recruitmentinfullmobile;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.net.miszewski.recruitmentinfullmobile.adapters.ItemsAdapter;

public class MainActivity extends AppCompatActivity {

    @Bind(R.id.recycler_view)
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        RecruitmentApp app = (RecruitmentApp) getApplication();

        recyclerView.setAdapter(new ItemsAdapter(app.getGroups(), this));
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @SuppressWarnings("SameReturnValue")
    public boolean rotateRecycleView(boolean clockWise) {
        recyclerView.animate().rotationBy(clockWise ? 360 : -360).setDuration(1000).start();
        return true;
    }
}
