package pl.net.miszewski.recruitmentinfullmobile.adapters;

import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import pl.net.miszewski.recruitmentinfullmobile.MainActivity;
import pl.net.miszewski.recruitmentinfullmobile.R;
import pl.net.miszewski.recruitmentinfullmobile.models.Group;
import pl.net.miszewski.recruitmentinfullmobile.models.Item;

public class ItemsAdapter extends ExpandableRecyclerAdapter<ItemsAdapter.GroupHolder, ItemsAdapter.ItemHolder> {

    private final List<Group> data;

    @NonNull
    private final MainActivity mainActivity;

    private final LayoutInflater inflater;

    public ItemsAdapter(@NonNull List<Group> groups, @NonNull MainActivity context) {
        super(groups);
        this.data = groups;
        this.mainActivity = context;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public GroupHolder onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View groupView = inflater.inflate(R.layout.list_item_parent, parentViewGroup, false);
        return new GroupHolder(groupView);
    }

    @Override
    public ItemHolder onCreateChildViewHolder(ViewGroup childViewGroup) {
        View itemView = inflater.inflate(R.layout.list_item_child, childViewGroup, false);
        return new ItemHolder(itemView);
    }

    @Override
    public void onBindParentViewHolder(GroupHolder parentViewHolder, int position, ParentListItem parentListItem) {
        parentViewHolder.name.setText(((Group) parentListItem).getName());
        parentViewHolder.isPositionEven = (position % 2) == 0;
    }

    @Override
    public void onBindChildViewHolder(ItemHolder childViewHolder, int position, Object childListItem) {
        childViewHolder.name.setText(((Item) childListItem).getName());
        childViewHolder.groupName = getGroupName(((Item) childListItem).getGroupId());
    }

    private String getGroupName(Integer groupId) {
        for (Group group : data) {
            if (groupId.equals(group.getId())) {
                return group.getName();
            }
        }
        return "";
    }

    class GroupHolder extends ParentViewHolder {

        @Bind(R.id.item_name)
        TextView name;

        boolean isPositionEven;

        public GroupHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnLongClickListener(v -> mainActivity.rotateRecycleView(isPositionEven));
        }
    }

    class ItemHolder extends ChildViewHolder {

        @Bind(R.id.item_name)
        TextView name;

        String groupName;

        public ItemHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnLongClickListener(v -> {

                AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(mainActivity);
                dialogBuilder
                        .setMessage(name.getText())
                        .setTitle(groupName)
                        .setPositiveButton("Ok", (dialog, which) -> {
                            dialog.dismiss();
                        }).show();
                return true;
            });
        }
    }
}
