package pl.net.miszewski.recruitmentinfullmobile.models;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.LinkedList;
import java.util.List;

public class Group implements ParentListItem {

    private final List<Item> itemList = new LinkedList<>();

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addItem(Item item) {
        itemList.add(item);
    }

    @Override
    public List<?> getChildItemList() {
        return itemList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
