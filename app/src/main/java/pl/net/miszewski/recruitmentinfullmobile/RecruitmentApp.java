package pl.net.miszewski.recruitmentinfullmobile;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pl.net.miszewski.recruitmentinfullmobile.models.Data;
import pl.net.miszewski.recruitmentinfullmobile.models.Group;
import pl.net.miszewski.recruitmentinfullmobile.models.Item;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

@SuppressWarnings("WeakerAccess")
public class RecruitmentApp extends Application {

    private static final String DATA_FILE_PATH = "data.json";

    private List<Group> groupList;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @Override
    public void onCreate() {
        super.onCreate();
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("assets/Roboto-Light.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );
        loadData();
    }

    private void loadData() {
        try {
            Reader reader = new InputStreamReader(getAssets().open(DATA_FILE_PATH));
            Gson gson = new Gson();
            Data data = gson.fromJson(reader, Data.class);

            Map<Integer, Group> groups = new HashMap<>();
            for (Group group : data.getGroups()) {
                groups.put(group.getId(), group);
            }

            for (Item item : data.getItems()) {
                groups.get(item.getGroupId()).addItem(item);
            }

            for (int i = data.getGroups().size() - 1; i > 0; i--) {
                Group group = data.getGroups().get(i);
                if (group.getChildItemList().size() == 0) {
                    data.getGroups().remove(i);
                }
            }
            groupList = data.getGroups();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Group> getGroups() {
        return groupList;
    }
}
