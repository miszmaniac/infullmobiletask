package pl.net.miszewski.recruitmentinfullmobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Item {

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("group_id")
    @Expose
    private Integer groupId;

    @SerializedName("name")
    @Expose
    private String name;

    public Integer getId() {
        return id;
    }

    public Integer getGroupId() {
        return groupId;
    }

    public String getName() {
        return name;
    }

}
