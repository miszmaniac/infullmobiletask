package pl.net.miszewski.recruitmentinfullmobile.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Data {

    @SerializedName("groups")
    @Expose
    private List<Group> groups = new ArrayList<>();

    @SerializedName("items")
    @Expose
    private List<Item> items = new ArrayList<>();

    public List<Group> getGroups() {
        return groups;
    }

    public List<Item> getItems() {
        return items;
    }

}
